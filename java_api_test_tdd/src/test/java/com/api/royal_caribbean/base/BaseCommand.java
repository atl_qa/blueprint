package com.api.royal_caribbean.base;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static io.restassured.RestAssured.given;

import java.io.File;
import java.util.List;


public abstract class BaseCommand extends BaseSetup {


	protected ValidatableResponse prepareGet(String path, List<Header> headerlist) {
	    log.info("Prepare to send GET request with path " + path + " and headers " + headerlist.toString());
	    try {
            return prepareGetDeleteWhen()
                .headers(new Headers(headerlist))
                .get(getBaseURI() + path)
                .then();}
	    catch (Exception e) {
	        log.error(e.toString());
	        return null;
        }
    }

	protected ValidatableResponse prepareDelete(String path) {
	    log.info ("Prepare to send DELETE request with path " + path);
	    try {
            return prepareGetDeleteWhen()
                .delete(envProperties.get("host").toString() + path)
                .then(); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

    protected ValidatableResponse preparePut(String path, String body) {
        log.info ("Prepare to send PUT request with path " + path + " and body " + body.toString());
        try {
            return preparePostPutWhen(body)
                .put(getBaseURI() + path)
                .then(); }
        catch (Exception e) {
            log.error (e.toString());
            return null;
        }
    }

    protected ValidatableResponse preparePost(String path, String body) {
	    log.info ("Prepare to send POST request with path " + path + " and body " + body.toString());
	    try {
            return preparePostPutWhen(body)
                .post(getBaseURI() + path)
                .then(); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

    protected ValidatableResponse preparePostWithFile(String path, File file, List<Header> headerlist) {
	    try {
            return preparePostWithFile(file, headerlist)
                .post(getBaseURI() + path)
                .then(); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

    protected String extractJsonPath (ValidatableResponse response, String path) {
	    try {
	        return response.extract().jsonPath().getString(path); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

    protected RequestSpecification preparePostPutWhen(String body) {
	    try {
	        return given()
                //.auth()
                //.basic(envProperties.get("username").toString(), envProperties.get("password").toString())
                .body(body)
                .log().all()
                .when(); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

    protected RequestSpecification prepareGetDeleteWhen() {
	    try {
            return given()
                //.auth()
                //.basic(envProperties.get("username").toString(), envProperties.get("password").toString())
                .log().all()
                .when(); }
	    catch (Exception e) {
	        log.error(e.toString());
	        return null;
        }
    }

    protected RequestSpecification preparePostWithFile (File file,  List<Header> headerlist) {
	    try {
	    return given ()
				//.baseUri(getBaseURI())
				//.port(getPort())
                .headers(new Headers(headerlist))
                .multiPart(file)
                .log().all()
                .when(); }
	    catch (Exception e) {
	        log.error (e.toString());
	        return null;
        }
    }

}
