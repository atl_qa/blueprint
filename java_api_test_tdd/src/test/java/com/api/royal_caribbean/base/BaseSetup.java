package com.api.royal_caribbean.base;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.testng.annotations.BeforeTest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

public abstract class BaseSetup {
	 Properties  envProperties = null;
	 protected Logger log = LogManager.getLogger("rootLogger");
	    //protected RestAssured restAssured;

	    public BaseSetup(){
	        getApplicationProperties();
	        //configureEnvironmentSetting();
	    }
	    public void getApplicationProperties(){
	        log.info ("Environment: " + System.getProperty("environment"));
	        InputStream is = null;
	        try {
	            this.envProperties = new Properties();
	            is = this.getClass().getResourceAsStream("/"+System.getProperty("environment")+".properties");
	            envProperties.load(is);
	        } catch (FileNotFoundException e) {
				log.error (e.toString());
	        } catch (IOException e) {
				log.error (e.toString());
	        }

	    }
	   // public void configureEnvironmentSetting(){
	        //RestAssured.baseURI = envProperties.get("host").toString();
	        //RestAssured.basePath = "/";
	        //RestAssured.port = Integer.parseInt(envProperties.get("port").toString());
	    //}

	    public String getBaseURI () {
	    	   return envProperties.get("host").toString();
		}

		public Integer getPort () {
	    	return Integer.parseInt(envProperties.get("port").toString());
		}

		public List<Header> getHeaders () {
			List<Header> headerlist = new ArrayList<Header>();
			headerlist.add(new Header("AppKey", "foZWsvCexUOsPiynpa0WJO8kuX3Re44R"));
			return headerlist;
		}

	//	private String buildURI (String host, String port) {
	//		if (port != null ) {
	//			return envProperties.get("host").toString() + ":" + envProperties.get("port").toString();
	//		} else {
	//			return envProperties.get("host").toString();
	//		}
	//	}

}
