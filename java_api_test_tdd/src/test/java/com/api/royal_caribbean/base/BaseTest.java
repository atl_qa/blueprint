package com.api.royal_caribbean.base;

import org.apache.logging.log4j.LogManager;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest extends BaseCommand {


    @BeforeMethod(alwaysRun = true)
    public void setUp (ITestContext ctx) {
        String testName = ctx.getCurrentXmlTest().getName();
        log = LogManager.getLogger(testName);
        log.info( "Start test ");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown () {
        log.info ("End test");
    }
}
