package com.api.royal_caribbean.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "json:target/cucumber/json/test/feature.json",
                "html:target/cucumber/html/"
        },
        features = "src/test/resources/features/StorageService",
        glue = {
//                "src/test/java/com/api/royal_caribbean/stepDefs"
//                "com.api.royal_caribbean.stepDefs"
        },
        tags = {"IntegrationTests"},
        strict = true
)
public class TestRunner {

}
