package com.api.royal_caribbean.stepDefs;

import com.api.royal_caribbean.base.BaseTest;
import com.api.royal_caribbean.config.EndPoints;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Random;


public class FrameworkTest extends BaseTest {


    @Test (priority = 1)
    public void uploadPhoto() {

        File file = new File ("src/test/resources/files/test1.png");

        ValidatableResponse response = preparePostWithFile(EndPoints.PHOTOS_ALL, file, getHeaders());
        String photoId = extractJsonPath (response, "payload.id");
        response.statusCode(201);

        response = prepareGet(EndPoints.PHOTOS_ALL + photoId, getHeaders());
        response.statusCode(200);

    }

    @Test (priority = 2)
    public void retrievePhotoWithNonExistingId () {
        Random rd = new Random();

        Integer id = rd.nextInt(10000000);
        ValidatableResponse response = prepareGet(EndPoints.PHOTOS_ALL + id.toString(), getHeaders());
        response.statusCode(404);

    }




}
