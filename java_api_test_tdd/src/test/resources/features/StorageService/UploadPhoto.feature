@IntegrationTest
Feature: Save photo in the persistent layer and retrieve photo from the persistent layer


Scenario: Save photo in the persistent layer

  Given HiThisIscharles

#  Given a POST request is made to <service-url>/photos
#  And the photo is attached as a multi-part file on that request
#  Then the photo is uploaded to our service
#  And the photo is persisted to local storage of the app
#  And a GUID is chosen to use as the name for the file

#Scenario: Retrieving existing photo from the persistent layer by id
#  Given a photo is in the persistent layer with photo id
#  And GET request is made to <service-url>/photos/id
#  Then that photo is returned as part of the response
#  And the response status is 200
#  And the photo is NOT an attachment on the response
#
#Scenario: Retrieving photo with non-existing id
#  Given I have non-existing photo id
#  When a GET request is made to <service-url>/photos/id
#  Then the response status is 404
#  And that photo is not returned as part of the response
#
#Scenario: Retrieving photo with size more than 100 Mb
#  Given I have photo with size more than 100Mb
#  And a POST request is made to <service-url>/photos
#  Then the response code is 400
#  And the photo is not uploaded to the server
